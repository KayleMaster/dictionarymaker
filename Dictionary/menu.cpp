
#include "stdafx.h"
#include "dict_functions.h"
#include <conio.h>
#include <ctype.h>
#include <stdlib.h>
#include "menu_functions.h"

int menu()
{
	system("cls");
	printf("< KayleMaster's Dictionary >\n");
	int choice = 0;
	if (language == 1)
		printf("1. Choose what language to read (currently ENGLISH)\n");
	else
		printf("1. Choose what language to read (currently BULGARIAN)\n");
	printf("2. Analyze file\n");
	printf("3. Save dictionary\n");
	printf("4. Load dictionary\n");
	printf("5. Save to a file\n");
	printf("6. Exit\n");
	do {
		choice = getChoice();
	} while (choice < 1 || choice >6);

	switch (choice)
	{
	case 1:
		system("cls");
		printf("Select a language:\n");
		printf("1. English\n");
		printf("2. Bulgarian\n");
		do {choice = getChoice();} while (choice < 1 || choice >2);
		language = choice;
		return 1;
		break;
	case 2:
		analyzeFile(language);
		return 1;
		break;
	case 3:
		return 1;
		break;
	case 4:
		return 1;
		break;
	case 5:
		printDictionaryFinal();
		return 1;
		break;
	case 6:
		return NULL;
		break;
	}
}

int getChoice()
{
	char chc;
	int chcc;
	do {
		chc = _getch();
	} while (isdigit(chc) == 0);
	chcc = chc - '0';
	return chcc;
}