#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include "dict_functions.h"
#include <ctype.h>
#include <stdlib.h>





void arrayToLower()
{
	for (int i = 0; i < actual_word_count; i++)
	{
		int y = 0;
		while (arr[i][y])
		{
			arr[i][y] = tolower(arr[i][y]);
			y++;
		}
	}
}



void firstPassSort()
{
	//char swap[MAX_WORD_LENGTH]; -- this gets corrupted as well
	char *swap = (char*)calloc(MAX_WORD_LENGTH, MAX_WORD_LENGTH * sizeof(char));
	int n = actual_word_count;
	for (int c = 0; c < (n - 1); c++)
	{
		for (int d = 0; d < n - c - 1; d++)
		{
			for (int letter = 1; letter < MAX_WORD_LENGTH; letter++)
			{
				if (0 < strcmp(arr[d], arr[d + 1]))
				{
					printf("\r");
					//swap = arr[d];
					strcpy(swap, arr[d]);
					//array[d] = array[d + 1];
					arr[d] = (char *)realloc(arr[d], (strlen(arr[d+1]) + 1) * sizeof(char));
					strcpy(arr[d], arr[d + 1]);
					//array[d + 1] = swap;
					arr[d+1] = (char *)realloc(arr[d+1], (strlen(swap) + 1) * sizeof(char));
					strcpy(arr[d + 1], swap);
					printf("Sorting progress: %.2lf%%", ((double)c / (double)actual_word_count) * 100);
					break;
				}
				else if ((int)arr[d][letter] == (int)arr[d + 1][letter])
				{
					continue;
				}
			}
		}
	}
	printf("\rSorting progress: 100%%    \n");
	free(swap);
}









void bubblesort(char **arrayy)
{
	int array[100], n, c, d, swap;

	printf("Enter number of elements\n");
	scanf("%d", &n);

	printf("Enter %d integers\n", n);

	for (c = 0; c < n; c++)
		scanf("%d", &array[c]);

	for (c = 0; c < (n - 1); c++)
	{
		for (d = 0; d < n - c - 1; d++)
		{
			if (array[d] > array[d + 1]) /* For decreasing order use < */
			{
				swap = array[d];
				array[d] = array[d + 1];
				array[d + 1] = swap;
			}
		}
	}

	printf("Sorted list in ascending order:\n");

	for (c = 0; c < n; c++)
		printf("%d\n", array[c]);
}