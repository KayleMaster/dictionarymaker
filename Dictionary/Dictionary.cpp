// Dictionary


#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include "dict_functions.h"
#include "menu_functions.h"
#include <conio.h>
#include <ctype.h>
#include <locale.h>
#define ENGLISH 1
#define BULGARIAN 2



//---------Word count stuff------------//
//unsigned long long word_count = 0;
char **arr = (char **)calloc(1, WORDS * sizeof(char *));
FILE *file_to_read_from;
char current_dir[_MAX_PATH];
int word_count_from_read_file = 0;
int language = BULGARIAN;
char filename_out[FILENAME_MAX];
//---------Word count stuff------------//

//(expression) ? true : false

int main()
{
	setlocale(LC_ALL, "Bulgarian");
	do {}  while (menu() == 1);
	int tests;
	
	//directoryCreate();
	//Base run
	analyzeFile(language);
		/*file_to_read_from = getFile();
		allocArray();
		for (int i = 97; i < 123; i++) 
		{
			finalStateMachine(i, file_to_read_from);
			arrayToLower();
			firstPassSort();
			//printArray();
			saveArray(i);
			//reallocArray(); -- redundant
		}
		flushArray();*/
		//Test array loading		
			/*allocArray();
			Sleep(2000);
			loadArray('a');
			printArray();
			Sleep(2000);
			reallocArray();
			flushArray();*/


		printDictionaryFinal();
		//if (!file) { system("cls"); printf("Could not open file. \n"); return 0; }

	//Reruns for memory leak testing
	scanf("%d", &tests);
	do {
		allocArray();
		for (int i = 97; i < 123; i++)
		{
			finalStateMachine(i, file_to_read_from);
			//arrayToLower();
			//firstPassSort(); --works but takes way too much time
			//printArray();
			saveArray(i);
			reallocArray();
		}
		flushArray();
		tests--;
	} while (tests);
	printf("Press any key to exit the program...\n");
	_getch();


	return 0;
}

void analyzeFile(int language)
{
	switch (language)
	{
	case 1: // English
		directoryCreate();
		file_to_read_from = getFile();
		allocArray();
		for (int i = 97; i < 123; i++)
		{
			finalStateMachine(i, file_to_read_from);
			arrayToLower();
			firstPassSort();
			saveArray(i);
		}
		flushArray();
		break;
	case 2: //Bulgarian
		directoryCreate();
		file_to_read_from = getFile();
		allocArray();
		for (int i = 224; i < 256; i++)
		{
			finalStateMachine(i, file_to_read_from);
			arrayToLower();
			firstPassSort();
			saveArray(i);
		}
		flushArray();
		break;
	}
}

void printDictionaryFinal()
{
	
	printf("Input filename to write the dictionary to: ");
	scanf("%s", filename_out);
	FILE *file = fopen(filename_out, "w");
	if (!file) {
		printf("File not found, try again..\n");
		printDictionaryFinal();
		return;
	}
	if (language == ENGLISH)
	{
		for (int i = 97; i < 123; i++)
		{
			allocArray();
			loadArray(i);
			fprintArray(file);
			flushArray();
		}
		fclose(file);
	}
	else
	{
		for (int i = 224; i < 256; i++)
		{
			allocArray();
			loadArray(i);
			fprintArray(file);
			flushArray();
		}
		fclose(file);
	}
}

void allocArray()
{
	for (int i = 0; i<WORDS; i++)
		arr[i] = (char *)calloc(1, sizeof(char)); 
}

void flushArray()
{
	for (int i = 0; i < WORDS; i++) {
			free(arr[i]);
	}
}

void reallocArray()
{
	if (actual_word_count < WORDS)
		for (int i = 0; i<=word_count; i++)
			arr[i] = (char *)realloc(arr[i], sizeof(char));
	else
		for (int i = 0; i < WORDS; i++)
			arr[i] = (char *)realloc(arr[i], sizeof(char));
}

void directoryCreate()
{
	GetCurrentDirectoryA(_MAX_PATH, current_dir); 
	strcat(current_dir, "\\DictionaryArrays");
	CreateDirectoryA(current_dir,NULL);
}

void saveArray(int letter)
{
	char array_dir[_MAX_PATH];
	SetCurrentDirectoryA(current_dir);	
	for (int i = 0; i < actual_word_count; i++)
	{
		printf("\r");
		//char charToStr[FILENAME_MAX];  -- ��������� �� ..
		char *charToStr = (char *)calloc(FILENAME_MAX, FILENAME_MAX*sizeof(char));
		//������� ���� - dictArrayForTheLetter
		char array_filename[FILENAME_MAX] = "dictArrayForTheLetter";
		memset(charToStr, '\0', sizeof(char)*strlen(charToStr));
		charToStr[0] = toupper(letter);
		//������� ���� - dictArrayForTheLetterA
		strcat(array_filename, charToStr);
		memset(charToStr, '\0', sizeof(char)*strlen(charToStr));
		sprintf(charToStr, "%d", i);
		//������� ���� - dictArrayForTheLetterA<word_number>
		strcat(array_filename, charToStr);
		//printf("%s\n", array_filename);
		FILE *f = fopen(array_filename, "w");
		fprintf(f, "%s", arr[i]);
		//fwrite(arr[i], sizeof(char), sizeof(arr[i]), f);
		fclose(f);
		free(charToStr);		
		printf("Saving progress for the letter [%c]: %lf%%", letter, ((double)i / (double)actual_word_count) * 100);
	}
	printf("\rSaving progress for the letter[%c]: 100%%        \n", letter);
	
}

void loadArray(int letter)
{	
	for (int i = 0; i<WORDS ; i++)
	{
		//�� �� �� �� ��������� �������
		char *temp = (char *)calloc(FILENAME_MAX, FILENAME_MAX * sizeof(char));
		char *charToStr = (char *)calloc(FILENAME_MAX, FILENAME_MAX * sizeof(char));
		//������� ���� - dictArrayForTheLetter
		char array_filename[FILENAME_MAX] = "dictArrayForTheLetter";
		memset(charToStr, '\0', sizeof(char)*strlen(charToStr));
		charToStr[0] = toupper(letter);
		//������� ���� - dictArrayForTheLetterA
		strcat(array_filename, charToStr);
		memset(charToStr, '\0', sizeof(char)*strlen(charToStr));
		sprintf(charToStr, "%d", i);
		//������� ���� - dictArrayForTheLetterA#word_number
		strcat(array_filename, charToStr);
		FILE *ifp = fopen(array_filename, "rb");
		if (!ifp)
		{
			free(charToStr); 
			free(temp);
			printf("Has read all words with the letter %c.\n", letter);
			actual_word_count = i;
			printf("Total words read: %d\n", i);
			break;
		}
		free(charToStr);
		free(temp);
		fscanf(ifp, "%s", temp);
		arr[i] = (char *)realloc(arr[i], (strlen(temp)+1) * sizeof(char));
		strcpy(arr[i], temp);
		fclose(ifp);
	}
}

void printArray()
{
		for (int i = 0; i < actual_word_count; i++)
		{
			printf("%s\n", arr[i]);
		}
}

void fprintArray(FILE *file_out)
{
	fprintf(file_out, "+------------------------------------------+\n");
	for (int i = 0; i < actual_word_count; i++)
	{
		fprintf(file_out,"%s\n", arr[i]);
	}
}





