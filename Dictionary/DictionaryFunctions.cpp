#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include "dict_functions.h"
#include <time.h>

void flushTempArray();
//---------File stuff------------------//
char filename[FILENAME_MAX];
FILE *file;
FILE *rand_file;
//---------Word count stuff------------//

//char **arr = (char **)calloc(word_length, word * sizeof(char *)); - Moved to the header

//---------Shared stuff----------------//
int letter_count = 0;
unsigned long long sentence_count = 0;
char temp_word[MAX_WORD_LENGTH];
unsigned long long word_count = 0, actual_word_count = 0;
unsigned long long word = 1024;
int word_length = 100;
unsigned int times_resized = 1;
int c, state = 0, count_this_word = 1;
//-------------------------------------//
//-------------------------------------//
const int count_words = 1, count_sentences = 2;
int check, first_line;
int word_overflow = 0,duplicate_word = 0;;
//-------------------------------------//



void finalStateMachine(int letter, FILE *file)
{
	c = NULL; state = 0, count_this_word = 1;
	word_count = 0; word_overflow = 0; duplicate_word = 0; actual_word_count = 0;
	flushTempArray();
	printf("Counting words with starting with: %c...\n", char(letter));
	
		times_resized = 1;
		letter_count = 0;
		rewind(file);
		while ((c = fgetc(file)))
		{
			//printf("%d\n", c);
			if (!(c == '.' || c == ',' || c == '?' || c == '!' || c == ':' || c == ';' || c == '/' || c == '\n' || c == EOF || c == ' ' || c == '"' || c == '=' || c == '>' || c == '<' || c == '\r' || c == '(' || c == ')'))
			{
				

				if (state == 0) //������� ������� ����� �� ������
				{
					//printf("%c", tolowerExtended(c));
					
					if ((tolower(c)) != letter)
					{
						count_this_word = 0;	
						state = 1;
						letter_count = 0;
						letter_count++;
					}
					else {
						count_this_word = 1;
						state = 1;
						letter_count = 0;
						++word_count;
						if (actual_word_count == WORDS - 2)
							{
								word_overflow = 1;
								printf("Words overflow detected..\n");
							}
							temp_word[letter_count++] = c;
					}
				}
				else { if (count_this_word && letter_count<=MAX_WORD_LENGTH) temp_word[letter_count++] = c; }
			}
			else
			{
				if (state == 1)
				{
					if (count_this_word && !word_overflow)
					{
						//����� ������ ����, ���� �� � ������� � ������
						//�� ���� ���� ��� � ��������? ��� ��� �� �����
						duplicate_word = 0;
						//������� ������ � ����� �����, �� �� �������� ����� ��������
						//��� �������� ������ � ������, ���� �� � ���� � ����, �������, �� �� ��� �
						int y = 0;
						while (temp_word[y])
						{
							temp_word[y] = tolower(temp_word[y]);
							y++;
						}
						//����������� ���� ��� � �������� ����
						for (int i = 0; i < actual_word_count; i++)
							if (!strcmp(temp_word, arr[i])) duplicate_word = 1;
						//��� �� ���, � ���������
						if (!duplicate_word)
						{
							int str_len = strlen(temp_word);
							arr[actual_word_count] = (char *)realloc(arr[actual_word_count], (letter_count + 1) * sizeof(char));						
							strcpy(arr[actual_word_count], temp_word);		
							actual_word_count++;
						}
					}
					//���������� ��������� ����� � ������
					flushTempArray();
					state = 0; letter_count = 0;
					count_this_word = 1;
				}
			} if (c == EOF) break;
		}
		printf("Words starting with <%c>: %llu\nActual word count: %llu\n", letter ,word_count, actual_word_count);
		
		flushTempArray();

}



void flushTempArray()
{
	memset(temp_word, '\0', sizeof(char)*MAX_WORD_LENGTH);
}

FILE *getFile()
{
	check = 0;
	do { check = inputFile(); } while (check == 0);
	return file;
}

int inputFile()
{
	printf("Input filename: ");
	scanf("%s", filename);
	file = fopen(filename, "r");
	if (!file) { system("cls"); printf("Could not open file. \n"); return 0; }
	else {
		printf("File successfully opened! \n");
		return 1;
	}
}

void makeTextFile()
{
	
	srand((unsigned int)time(NULL));
	const int filename_length = 12;
	char name[filename_length];
	for (int p = 0; p < filename_length - 5; p++)
		switch (rand() % 3)
		{
		case 0:
			name[p] = 97 + rand() % (122 + 1 - 97);
			break;
		case 1:
			name[p] = 48 + rand() % (57 + 1 - 48);
			break;
		case 2:
			name[p] = 65 + rand() % (90 + 1 - 65);
			break;
		}

	name[7] = '\0';
	strcat(name, ".txt\0");

	printf("%s\n", name);

	rand_file = fopen(name, "w");
	if (!rand_file) { printf("Impossible\n"); }

}

