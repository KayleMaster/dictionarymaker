#include "stdafx.h"
#include <stdio.h>

#define WORDS 65536
#define MAX_WORD_LENGTH 32

//---------Prototypes------------------//
	//For Dictionary.cpp
		void 
			allocArray(), 
			flushArray(),
			bubblesort(char **array), 
			printArray(), 
			reallocArray(),
			saveArray(int letter),
			loadArray(int letter),
			printDictionaryFinal(),
			fprintArray(FILE *file_out),
			directoryCreate(),
			finalStateMachine(int letter, FILE *file),
			analyzeFile(int language),
			makeTextFile();
		FILE 
			*getFile();
		int 
			inputFile();			
	//From DictionarySort.cpp
		void 
			bubblesort(char **array),
			firstPassSort(),
			arrayToLower();
	//From DictionaryFunctions.cpp
		extern unsigned long long 
			word_count, 
			actual_word_count;
		extern int
			language;
		extern char 
			filename_out[FILENAME_MAX];
	//From Dictionary.cpp
		extern char **arr;
		extern char current_dir[FILENAME_MAX];